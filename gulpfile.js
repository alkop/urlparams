var gulp = require('gulp');
var nugetpack = require('gulp-nuget-pack');

gulp.task('nuget-pack', function(callback) {
    nugetpack({
            id: "URLParams",
            title: "URL Params",
            version: "1.1.0",
            authors: "Alexander Kopylov",
            description: "JavaScript Library for manage URL parametes",
            summary: "Library provide common functionality for ADD, UPDATE, REMOVE url parameters.",
            projectUrl: "https://bitbucket.org/alkop/urlparams",
            iconUrl: "http://alkop.bitbucket.org/urlparams/content/img/logo.png",
            licenseUrl: "http://opensource.org/licenses/MIT",
            copyright: "Copyright � 2016 Alexander Kopylov",
            requireLicenseAcceptance: false,
            tags: "JavaScript URL Parameters",
            outputDir: "out"
        },
 
        [
            {src: "urlparams.js", dest: "content/Scripts/urlparams.js"},
            {src: "urlparams.min.js", dest: "content/Scripts/urlparams.min.js"},
            {src: "urlparams.min.js.map", dest: "content/Scripts/urlparams.min.js.map"}
        ],
 
        callback
    );
});