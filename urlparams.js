﻿/*!
 * URLParams JavaScript Library v1.1
 * https://bitbucket.org/alkop/urlparams
 *
 * Copyright 2015 Alexander Kopylov
 *
 * Released under the MIT license
 */
var URL = {
    current: location.href,
    Parameters: {
        get: function (param) {
            var splitedUrl = URL.Utils.splitUrlParams(URL.current, param);
            if (splitedUrl.hasSeekingParam) {
                for (var index = 0; index < splitedUrl.keyValuePairs.length; index++) {
                    if (splitedUrl.keyValuePairs[index].isSeeking) {
                        return splitedUrl.keyValuePairs[index].val;
                    }
                }
            }
            return undefined;
        },
        add: function (param, val) {
            if (URL.Helpers.isEmpty(param, val)) {
                return false;
            }
            var url = URL.Utils.removeHash();
            if (url.indexOf("?") === -1) {
                return url + "?" + param + "=" + val;
            }
            var splitedUrl = URL.Utils.splitUrlParams(url, param);
            if (splitedUrl.hasSeekingParam === false) {
                if (!splitedUrl.params) {
                    return splitedUrl.baseUrl + "?" + param + "=" + val;
                }
                return url + "&" + param + "=" + val;
            }
            return false;
        },
        remove: function (param) {
            var url = URL.current;
            if (url.indexOf("?") === -1) {
                return false;
            }
            var splitedUrl = URL.Utils.splitUrlParams(url, param);
            if (splitedUrl.hasSeekingParam) {
                url = splitedUrl.baseUrl + "?";
                for (var index = 0; index < splitedUrl.keyValuePairs.length; index++) {
                    if (!splitedUrl.keyValuePairs[index].isSeeking) {
                        url = url + splitedUrl.params[index] + "&";
                    }
                }
                if (splitedUrl.params.length > 1) {
                    url = url.slice(0, url.length - 1);
                }
                return url;
            }
            return false;
        },
        update: function (param, val) {
            if (URL.Helpers.isEmpty(param, val)) {
                return false;
            }
            var url = URL.Utils.removeHash();
            if (url.indexOf("?") === -1) {
                return false;
            }
            var splitedUrl = URL.Utils.splitUrlParams(url, param);
            if (splitedUrl.hasSeekingParam) {
                url = splitedUrl.baseUrl + "?";
                for (var index = 0; index < splitedUrl.keyValuePairs.length; index++) {
                    if (splitedUrl.keyValuePairs[index].isSeeking) {
                        url = url + param + "=" + val + "&";
                    } else {
                        url = url + splitedUrl.params[index] + "&";
                    }
                }
                url = url.slice(0, url.length - 1);
                return url;
            }
            return false;
        }
    },
    Utils: {
        removeHash: function (url) {
            url = url == undefined ? URL.current : url;
            return url.split('#')[0];
        },
        parseUrl: function (url) {
            var parser = document.createElement('a');
            parser.href = url == undefined ? URL.current : url;
            return {
                protocol: parser.protocol,
                hostname: parser.hostname,
                port: parser.port,
                pathname: parser.pathname,
                params: parser.search,
                hash: parser.hash,
                host: parser.host
            }
        },
        splitUrlParams: function (url, seekingParam) {
            url = url == undefined ? URL.current : url;
            var splitedUrl = url.split("?");
            if (splitedUrl.length === 1) {
                return {
                    baseUrl: splitedUrl[0],
                    hasSeekingParam: false
                };
            } else if (splitedUrl.length === 2) {
                var result = {
                    baseUrl: splitedUrl[0],
                    paramsUrl: splitedUrl[1],
                    params: splitedUrl[1].split(/[&;]/g),
                    keyValuePairs: [],
                    hasSeekingParam: false
                };
                for (var paramIndex = 0; paramIndex < result.params.length; paramIndex++) {
                    var keyValuePair = result.params[paramIndex].split("=");
                    if (keyValuePair.length === 2) {
                        result.keyValuePairs.push({ key: keyValuePair[0], val: keyValuePair[1], isSeeking: keyValuePair[0] === seekingParam });
                        result.hasSeekingParam = result.hasSeekingParam || keyValuePair[0] === seekingParam;
                    }
                }
                return result;
            }
            return false;
        }
    },
    Helpers: {
        isEmpty: function () {
            for (var i = 0; i < arguments.length; i++) {
                var str = arguments[i];
                if (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null) {
                    return true;
                }
            }
            return false;
        }
    }
}
