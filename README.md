URLParams - JavaScript Library for manage URL parametes
=======================================================

[![Build status][appveyor img]][appveyor]
[![NuGet][nuget img]][nuget]
[![License][license img]][license]

* Simple and flexible API
* Allow you to GET, ADD, UPDATE or REMOVE parameters from url
* Available at NuGet: 
    * `PM> Install-Package URLParams`
* Fully documented at [Wiki Page][WikiHome]

[WikiHome]: https://bitbucket.org/alkop/urlparams/wiki/

[appveyor]:https://ci.appveyor.com/project/AlexanderKopylov/urlparams/branch/master
[appveyor img]:https://ci.appveyor.com/api/projects/status/vpp8yiidmeb2e34m/branch/master

[nuget]:https://www.nuget.org/packages/urlparams
[nuget img]:https://img.shields.io/nuget/v/urlparams.svg

[license]:LICENSE.md
[license img]:https://img.shields.io/badge/license-MIT-red.svg